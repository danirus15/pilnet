<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="kontak";?>
<?php include "includes/head.php";?>

<body>
	<?php include "includes/header.php";?>
	<div class="container container_nowp2">
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Kontak</a>
		</div>
		<h1 class="title title2">Kontak</h1>
	</div>
	<!-- s:peta -->
	<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d8160886.443161523!2d121.9695609132859!3d-2.8717490758759427!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1420684759428" width="1200" height="300" frameborder="0" style="border:0" class="peta2"></iframe>
	<!-- e:peta -->
	<div class="clearfix pt30"></div>
	<!-- s:kontak -->
	<div class="container">
		<div class="k_left">
			<h2>Pilnet</h2>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate odio quis urna ornare pulvinar. Nullam luctus urna mollis mollis tristique. Suspendisse tristique erat at consequat bibendum. Pellentesque nibh velit, dictum sollicitudin lectus et, ornare tincidunt magna. Nullam sit amet interdum enim. Nulla interdum mattis ipsum, sed mollis lacus ornare non. In faucibus sit amet leo id ultrices. Quisque accumsan, ex vel laoreet rhoncus, sem erat elementum est, a pellentesque lectus elit quis neque. Donec pretium cursus mi, at maximus lacus dictum non. Pellentesque rutrum metus congue orci maximus iaculis.
			<div class="clearfix pt30"></div>
			<form action="#" class="pt20">
				<h2 class="pb10">Kontak</h2>
				<h6 class="pb5">Nama</h6>
				<input type="text" class="input100">
				<div class="clearfix pt10"></div>
				<h6 class="pb5">Email</h6>
				<input type="text" class="input100">
				<div class="clearfix pt10"></div>
				<h6 class="pb5">Jenis Pertanyaan</h6>
				<select name="" id="" class="input100">
					<option value=""></option>
					<option value=""></option>
				</select>
				<div class="clearfix pt10"></div>
				<h6 class="pb5">No. Tlp</h6>
				<input type="text" class="input100">
				<div class="clearfix pt10"></div>
				<h6 class="pb5">Isi Pengaduan</h6>
				<textarea name="" id="" class="textarea100 h100"></textarea>
				<div class="clearfix pt10"></div>
				<input type="submit"  class="btn_save" value="Kirim">
			</form>
		</div>
		<div class="k_right">
			<strong>Pilnet</strong><br>
			Kantor Sekretariat Public Interests Lawyer Network (PIL-Net)<br>
			Jl. Siaga II No.31<br>
			Pejaten Barat, Pasar Minggu<br>
			Jakarta Selatan<br>
			INDONESIA – 12510<br>
			<br>
			Tel: +62 21 7972662, 79192564<br>
			Fax: +62 21 79192519<br>
			E-mail : pilnetindonesia@elsam.or.id<br>
			Web page: pengacarapublik.or.id
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- e:kontak -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>