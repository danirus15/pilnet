<div class="clearfix pt30"></div>
<div id="footer">
	<div class="container">
		<div class="foot1">
			<div class="logo"><a href="#"><img src="img/logo.png" alt=""></a></div>
			<div class="sosmed">
				<a href="#"></a>
			</div>
		</div>
		<div class="clearfix pt30"></div>
		<div class="foot2">
			<a href="#">Home</a>
			<a href="#">Berita & Kasus </a>
			<a href="#">Tim</a>
			<a href="#">Kampanye</a>
			<a href="#">Publikasi</a>
			<a href="#">Advokasi Kebijakan</a>
			<a href="#">Badan Pengampu</a>
			<a href="#">Kontak</a>
		</div>
		<div class="clearfix pt10"></div>
		<div class="foot3">Copyright © 2015 PilNet Indonesia    -  Allright reserved</div>
	</div>
</div>