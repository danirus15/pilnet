<div id="header">
	<div class="container">
		<a href="index.php" class="logo"><img src="img/logo.png" alt="Pilnet"></a>
		<ul id="ddmenu" class="sf-menu" >
			<li class="selected">
				<a href="index.php">Beranda</a>
			</li>
			<li>
				<a href="berita.php">Berita</a>
				<ul>
					<li>
						<a href="berita.php?t=Hutan">Hutan</a>
					</li>
					<li>
						<a href="berita.php?t=Perkebunan">Perkebunan</a>
					</li>
					<li>
						<a href="berita.php?t=Pertambangan">Pertambangan</a>
					</li>
					<li>
						<a href="berita.php?t=Kebebasan Ekspresi">Kebebasan Ekspresi</a>
					</li>
					<li>
						<a href="berita.php?t=Isu Lainnya">Isu Lainnya</a>
					</li>
				</ul>
			</li>
			<li>
				<a>Pengelola</a>
				<ul>
					<li>
						<a href="struktur.php">Struktur Organisasi</a>
					</li>
					<li>
						<a href="direksi.php">Direksi</a>
					</li>
					<li>
						<a href="anggota.php">Anggota</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="kampanye.php">Kampanye</a>
			</li>
			<li>
				<a href="publikasi.php">Publikasi</a>
			</li>
			<li>
				<a href="kasus.php">Kasus</a>
			</li>
			<li>
				<a href="kontak.php">Kontak</a>
			</li>
		</ul>
	</div>
	<div id="nav_menu">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div id="nav_menu_close"><span>+</span></div>
</div>