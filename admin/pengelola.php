<!DOCTYPE html>
<html lang="en" class="app">
<?php $page="pengelola";?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <h3 class="m-b-xs text-black fl">Pengelola > Struktur Organisasi</h3>
                      <a href="pengelola_add.php" class="btn btn-s-md btn-primary btn-rounded fr">Tambah</a>
                      <div class="clearfix"></div>

                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                   <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="15%">Nama</th>
                        <th width="10%">Lembaga</th>
                        <th width="10%">Jabatan</td>
                        <th width="10%">Daerah</th>
                        <th width="10%">Email</th>
                        <th width="15%">Phone</th>
                        <th width="20%">Specialist</th>
                        <th width="25%">Photo</th>
                        <th width="20%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Raka</td>
                        <td>Elsam</td>
                        <td>Direksi</td>
                        <td>Jakarta</td>
                        <td>raka@gmail.com</td>
                        <td>0819324442283</td>
                        <td>Perdata</td>
                        <td><img src="images/a8.png" class="img_foto"></td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-original-title="Delete"><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>Raka</td>
                        <td>Elsam</td>
                        <td>Direksi</td>
                        <td>Jakarta</td>
                        <td>raka@gmail.com</td>
                        <td>0819324442283</td>
                        <td>Perdata</td>
                        <td><img src="images/a8.png" class="img_foto"></td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-original-title="Delete"><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>