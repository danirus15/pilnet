<?php
  if($page=="home") {$home="active";}
  elseif($page=="berita") {$berita="active";}
?>


<aside class="bg-light aside-md hidden-print" id="nav">          
  <section class="vbox">
    <section class="w-f scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
        <div class="clearfix wrapper dk nav-user hidden-xs">
          <div class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="thumb avatar pull-left m-r">                        
                <img src="images/m1.jpg" class="dker" alt="...">
                <i class="on md b-black"></i>
              </span>
              <span class="hidden-nav-xs clear">
                <span class="block m-t-xs">
                  <strong class="font-bold text-lt">Pepe Rama</strong> 
                  <b class="caret"></b>
                </span>
                <span class="text-muted text-xs block">Pengacara</span>
              </span>
            </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">                      
              <li>
                <span class="arrow top hidden-nav-xs"></span>
                <a href="#">Settings</a>
              </li>
              <li>
                <a href="profile.html">Profile</a>
              </li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">3</span>
                  Notifications
                </a>
              </li>
              <li>
                <a href="docs.html">Help</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="modal.lockme.html" data-toggle="ajaxModal" >Logout</a>
              </li>
            </ul>
          </div>
        </div>                


        <!-- nav -->                 
        <nav class="nav-primary hidden-xs">
          <ul class="nav nav-main" data-ride="collapse">
            <li  class="<?php echo $home;?>">
              <a href="index.php" class="auto">
                <i class="i i-grid2 icon">
                </i>
                <span class="font-bold">Dashboard</span>
              </a>
            </li>
            <li class="<?php echo $pengaduan;?>">
              <a href="pengaduan.php" class="auto">
                <i class="i i-chat2 icon">
                </i>
                <span class="font-bold">Pengaduan</span>
              </a>
            </li>
            <li  class="<?php echo $berita;?>">
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>
                <i class="i i-stack icon">
                </i>
                <span class="font-bold">Berita</span>
              </a>
              <ul class="nav dk">
                <li  class="<?php echo $berita;?>">
                  <a href="berita.php" class="auto">                                                        
                    <i class="i i-dot"></i>
                    <span>Hutan</span>
                  </a>
                </li>
                <li >
                  <a href="berita.php" class="auto">                                                        
                    <i class="i i-dot"></i>

                    <span>Perkebunan</span>
                  </a>
                </li>
                <li >
                  <a href="berita.php" class="auto">                                                        
                    <i class="i i-dot"></i>

                    <span>Pertambangan</span>
                  </a>
                </li>
                <li >
                  <a href="berita.php" class="auto">                                                        
                    <i class="i i-dot"></i>

                    <span>Kebebasan Berekspresi</span>
                  </a>
                </li>
                <li >
                  <a href="berita.php" class="auto">                                                        
                    <i class="i i-dot"></i>

                    <span>Isu Lainnya</span>
                  </a>
                </li>
              </ul>
            </li>
            <li>
                <a href="pengelola.php" class="auto">
                <i class="fa fa-group icon">
                </i>
                <span class="font-bold">Pengelola</span>
              </a>
              <!-- <ul class="nav dk">
                <li  class="<?php echo $pengelola;?>">
                  <a href="pengelola.php" class="auto">                                                        
                    <i class="i i-dot"></i>
                    <span>Struktur Organisasi</span>
                  </a>
                </li>
                <li >
                  <a href="pengelola.php" class="auto">                                                        
                    <i class="i i-dot"></i>
                    <span>Direksi</span>
                  </a>
                </li>
                <li >
                  <a href="pengelola.php" class="auto">                                                        
                    <i class="i i-dot"></i>
                    <span>Anggota</span>
                  </a>
                </li>
              </ul> -->
            </li>
            <li>
              <a href="kampanye.php" class="auto">
                <i class="i i-layer icon">
                </i>
                <span class="font-bold">Kampanye</span>
              </a>
            </li>
            <li>
              <a href="publikasi.php" class="auto">
                <i class="i i-publish icon">
                </i>
                <span class="font-bold">Publikasi</span>
              </a>
            </li>
            <li>
              <a href="kasus.php" class="auto">
                <i class="i i-docs icon">
                </i>
                <span class="font-bold">Kasus</span>
              </a>
            </li>
            <li>
              <a href="kontak.php" class="auto">
                <i class="fa fa-info-circle icon">
                </i>
                <span class="font-bold">Kontak</span>
              </a>
            </li>
            <li>
              <a href="pengaduan.php" class="auto">
                <i class="fa fa-folder-open icon">
                </i>
                <span class="font-bold">File Manager</span>
              </a>
            </li>
            <li>
                <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="i i-circle-sm-o text"></i>
                  <i class="i i-circle-sm text-active"></i>
                </span>
                <i class="fa fa-group icon">
                </i>
                <span class="font-bold">Admin Member</span>
              </a>
              <ul class="nav dk">
                <li  class="<?php echo $pengelola;?>">
                  <a href="group.php" class="auto">                                                        
                    <i class="i i-dot"></i>
                    <span>Group</span>
                  </a>
                </li>
                <li >
                  <a href="member.php" class="auto">                                                        
                    <i class="i i-dot"></i>
                    <span>Member</span>
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a href="setting.php" class="auto">
                <i class="fa fa-folder-open icon">
                </i>
                <span class="font-bold">Setting</span>
              </a>
            </li>
            
            
          </ul>
          <div class="line dk hidden-nav-xs"></div>
        </nav>
        <!-- / nav -->
      </div>
    </section>
    
    <footer class="footer hidden-xs no-padder text-center-nav-xs">
      <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-icon icon-muted btn-inactive pull-right m-l-xs m-r-xs hidden-nav-xs">
        <i class="i i-logout"></i>
      </a>
      <a href="#nav" data-toggle="class:nav-xs" class="btn btn-icon icon-muted btn-inactive m-l-xs m-r-xs">
        <i class="i i-circleleft text"></i>
        <i class="i i-circleright text-active"></i>
      </a>
    </footer>
  </section>
</aside>