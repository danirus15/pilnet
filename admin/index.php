<!DOCTYPE html>
<html lang="en" class="app">
<?php $page="home";?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Dashboard</h3>
                      <small>Selamat Datang Kembali Pepe Rama, <i class="fa fa-map-marker fa-lg text-primary"></i> Jakarta Selatan</small>
                    </div>
                  </section>
                  <!-- s:content --> 
                  <div class="col-sm-6 panel panel-default">
                      <h5><b>Daftar Pengaduan</b></h5>
                      <div class="table-responsive">
                        <table class="table table-striped m-b-none">
                          <thead>
                            <tr>
                              <th width="15%">Nama</th>
                              <th width="70%">Isi</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                Raka, raka@gmail.com
                                <br>July 24, 2013
                              </td>
                              <td>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus odio et odio semper ullamcorper. Vestibulum eu vulputate velit. Donec ve
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Raka, raka@gmail.com
                                <br>July 24, 2013
                              </td>
                              <td>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus odio et odio semper ullamcorper. Vestibulum eu vulputate velit. Donec ve
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Raka, raka@gmail.com
                                <br>July 24, 2013
                              </td>
                              <td>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus odio et odio semper ullamcorper. Vestibulum eu vulputate velit. Donec ve
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Raka, raka@gmail.com
                                <br>July 24, 2013
                              </td>
                              <td>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tempus odio et odio semper ullamcorper. Vestibulum eu vulputate velit. Donec ve
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-sm-6 panel panel-default">
                      <h5><b>Berita Terbaru</b></h5>
                      <div class="table-responsive">
                        <table class="table table-striped m-b-none">
                          <thead>
                            <tr>
                              <th width="25%">Penulis</th>
                              <th width="70%">Isi</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                Raka,
                                <br>July 24, 2013
                              </td>
                              <td>
                                Berita Hutan<br>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Raka,
                                <br>July 24, 2013
                              </td>
                              <td>
                                Berita Pengaduan<br>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                Raka,
                                <br>July 24, 2013
                              </td>
                              <td>
                                Berita Hutan<br>
                                <b>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi top</b><br>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <hr size="1" color="#ccc">
                      <h5><b>Anggota Baru</b></h5>
                      <div class="table-responsive">
                        <table class="table table-striped m-b-none">
                          <thead>
                            <tr>
                              <th width="25%">Nama</th>
                              <th width="25%">Lembaga</th>
                              <th width="25%">Jabatan</th>
                              <th width="25%">Lokasi</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Raka</td>
                              <td>Elsam</td>
                              <td>Direksi</td>
                              <td>Jakarta</td>
                            </tr>
                            <tr>
                              <td>Raka</td>
                              <td>Elsam</td>
                              <td>Direksi</td>
                              <td>Jakarta</td>
                            </tr>
                            <tr>
                              <td>Raka</td>
                              <td>Elsam</td>
                              <td>Direksi</td>
                              <td>Jakarta</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  <!-- e:content -->
                </section>
              </section>
            </section>
            
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>