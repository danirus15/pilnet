<!DOCTYPE html>
<html lang="en" class="app">
<?php $page="group";?>
<?php include "includes/head.php";?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php";?>
    <section>
      <section class="hbox stretch">
        <!-- .aside -->
        <?php include "includes/menu.php";?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="title_page">
                      <h3 class="m-b-xs text-black fl">Group</h3>
                      <a href="group_add.php" class="btn btn-s-md btn-primary btn-rounded fr">Tambah</a>
                      <div class="clearfix"></div>

                    </div>
                  </section>
                  <div class="clearfix"></div>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                   <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="60%">Nama</th>
                        <th width="10%">Akses</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Super Admin</td>
                        <td>All</td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" data-original-title="Delete"><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                  </section>
                  <!-- e:content --> 
                </section>
              </section>

            </section>

          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
      </section>
    </section>
  </section>
  <?php include "includes/js.php";?>
</body>
</html>