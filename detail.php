<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="berita";?>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/header.php";?>
	<!-- s:detail -->
	<div class="container container_nowp">
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Berita & Kasus</a> /
			<a href="#">Pertambangan</a>
		</div>
		<h1>[Karang Mendapo] Karang Mendapo Melawan Ketidakadilan – Pledooi Rusdi</h1>
		<div class="date">Sarolangun, 01 Juli 2009</div>
		<div class="detail_left">
			<div class="pic">
				<img src="img/07.jpg" alt="">
				<span class="caption">Sehingga persidangan ini, tidaklah semata-mata persidangan terhadap Terdakwa</span>
			</div>
			<div class="share_box">
				<div id="sticky1">
					<strong>SHARE</strong>
					<a href="#"><img src="img/sos_fb.png" alt=""></a>
					<a href="#"><img src="img/sos_tw.png" alt=""></a>
					<a href="#"><img src="img/sos_gplus.png" alt=""></a>
					<a href="#"><img src="img/sos_pin.png" alt=""></a>
				</div>
			</div>
			<div class="text_detail">
				Persidangan atas Muhammad Rusdi alias Bujang Ajang Bin H. Muhamad bukanlah persidangan terakhir bagi kaum tani dan kaum miskin yang memperjuangkan hak-haknya.
				<br><br>
				Bersamaan dengan persidangan kasus ini, 9 (sembilan) orang petani di Pati – Jawa Tengah tengah menunggu vonis atas perjuangannya menolak pembangunan Pabrik Semen Gresik yang akan melanggar  hak atas lingkungan yang sehat dan hak ekonomi, sosial dan budayanya; 6 (enam) orang petani di Kendal – Jawa Tengah dinyatakan bersalah atas perjuangannya menolak diperpanjangnya perkebunan besar di wilayah mereka yang telah membawa kemiskinan pada masyarakat sekitarnya; 3 (tiga) orang yang terdiri dari Kepala Desa, Kepala Dusun dan Kepala Adat di Kabupaten Melawi-Kalimantan Barat diperiksa kepolisian karena  menolak eksplorasi batubara di wilayah adatnya; 4 (empat) orang di Kabupaten Agam – Sumatera Barat sedang dalam proses penuntutan atas penolakan terhadap perkebunan besar di wilayahnya.
				<br><br>
				Sehingga persidangan ini, tidaklah semata-mata persidangan terhadap Terdakwa, tetapi juga persidangan kepada kaum tani dan orang miskin di Indonesia.
				<br><br>
				Persidangan atas Muhammad Rusdi alias Bujang Ajang Bin H. Muhamad bukanlah persidangan terakhir bagi kaum tani dan kaum miskin yang memperjuangkan hak-haknya.
				<br><br>
				Bersamaan dengan persidangan kasus ini, 9 (sembilan) orang petani di Pati – Jawa Tengah tengah menunggu vonis atas perjuangannya menolak pembangunan Pabrik Semen Gresik yang akan melanggar  hak atas lingkungan yang sehat dan hak ekonomi, sosial dan budayanya; 6 (enam) orang petani di Kendal – Jawa Tengah dinyatakan bersalah atas perjuangannya menolak diperpanjangnya perkebunan besar di wilayah mereka yang telah membawa kemiskinan pada masyarakat sekitarnya; 3 (tiga) orang yang terdiri dari Kepala Desa, Kepala Dusun dan Kepala Adat di Kabupaten Melawi-Kalimantan Barat diperiksa kepolisian karena  menolak eksplorasi batubara di wilayah adatnya; 4 (empat) orang di Kabupaten Agam – Sumatera Barat sedang dalam proses penuntutan atas penolakan terhadap perkebunan besar di wilayahnya.
				<br><br>
				Sehingga persidangan ini, tidaklah semata-mata persidangan terhadap Terdakwa, tetapi juga persidangan kepada kaum tani dan orang miskin di Indonesia.
				<div class="down_artikel">

					<a href="#">
						<span class="file">File Attachment 1</span>
						<div class="fr">
							<img src="img/ico_download.png" alt="">
							<span>Download</span>
						</div>
					</a>
					<a href="#">
						<span class="file">File Attachment 1</span>
						<div class="fr">
							<img src="img/ico_download.png" alt="">
							<span>Download</span>
						</div>
					</a>
					<a href="#">
						<span class="file">File Attachment 1</span>
						<div class="fr">
							<img src="img/ico_download.png" alt="">
							<span>Download</span>
						</div>
					</a>
				</div>
			</div>

		</div>
		<div class="detail_right">
			<div class="title2 f22 pb10">Berita Terbaru</div>
			<div class="list_berita list_berita_detail">
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/02.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/img_default.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/04.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/05.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
		</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- e:detail -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>