<?php error_reporting(E_ALL & ~E_NOTICE);?>
<!DOCTYPE html>
<html>
<?php $page=="pengelola";?>
<?php include "includes/head.php";?>

<body class="body_pop">
	<div class="pop_bg"></div>
	<div class="pop_container pop_container600">
    	<div id="pop_wrap">
    		<div class="pop_content">
				<div class="header">
					<h3>Member</h3>
		            <a class="close_pc close_box_in" href="" target="_parent">
		                <span>+</span>
		            </a>
		        </div>
		        <div class="pop_content2">
		        	<div class="pic_member">
		        		<img src="img/m3.jpg" alt="">
		        	</div>
		        	<div class="text">
		        		<h1>Joko Suseno</h1>
		        		<h5>Direksi</h5>
		        		<div class="clearfix pt10"></div>
		        		<h4>Biografi</h4>
		        		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sapien neque, molestie vel pulvinar sit amet, pretium vitae mauris. Nulla facilisi. Aenean quis massa quis dolor volutpat mattis. Duis in consectetur ante. Donec quis mi maximus, laoreet odio et, aliquet elit
		        		<div class="clearfix pt15"></div>
		        		<h4>Archivement</h4>
		        		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sapien neque
		        		<ul>
		        			<li>Lorem ipsum dolor sit amet</li>
		        			<li>consectetur adipiscing elit. Sed sapien neque</li>
		        		</ul>
		        	</div>
		        	<div class="clearfix"></div>
		        </div>
		        <a href="#" class="m_n_left"><img src="img/nav_left.png" alt=""></a>
		        <a href="#" class="m_n_right"><img src="img/nav_right.png" alt=""></a>
	        </div>
	    </div>
	</div>
</body>
<?php include "includes/js.php";?>
</html>