<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="pengaduan";?>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/header.php";?>
	<!-- s:detail -->
	<div class="container container_nowp">
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Pengaduan</a>
		</div>
		<div class="clearfix"></div>
		<div class="title">Pengaduan</div>
	</div>
	<div class="header_page">
		<img src="img/h_pengaduan.jpg" alt="">
	</div>
	<div class="form_peng">
		<div class="container">
			Silahkan isi form untuk mengirimkan pengaduan:

			<form action="#" class="pt20">
				<div class="fl w450">
					<h6>Nama</h6>
					<input type="text" class="input100">
				</div>
				<div class="fl w450">
					<h6>Email</h6>
					<input type="text" class="input100">
				</div>
				<div class="clearfix pt10"></div>
				<div class="fl w450">
					<h6>Jenis Pengaduan</h6>
					<select name="" id="" class="input100">
						<option value=""></option>
						<option value=""></option>
					</select>
				</div>
				<div class="fl w450">
					<h6>No. Tlp</h6>
					<input type="text" class="input100">
				</div>
				<div class="clearfix pt10"></div>
				<h6>Isi Pengaduan</h6>
				<textarea name="" id="" class="textarea100 h100"></textarea>
				<div class="clearfix pt10"></div>
				<input type="submit"  class="btn_save" value="Kirim">
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="container ">
		<div class="clearfix pt20"></div>
		<div class="tw_b" align="center">
			Pengirim pengaduan melalui twitter:
			<div class="clearfix pt10"></div>
			<a class="twitter-timeline" href="https://twitter.com/twitter" data-widget-id="556978240229609473">Tweets by @twitter</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div>
	</div>
	<!-- e:detail -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>