<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="pengelola";?>
<?php include "includes/head.php";?>

<body>
	<?php include "includes/header.php";?>
	<div class="container container_nowp2">
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Pengelola</a> /
			<a href="#">Anggota</a>
		</div>
		<h1 class="title title2 fl">Anggota</h1>
		<select name="" id="" class="pilihberita">
			<option value="">Semua Wilayah</option>
			<option value="">Jawa Barat</option>
			<option value="">Jawa Timur</option>
			<option value="">DKI Jakarta</option>
			<option value="">Kalimantan Selatan</option>
			<option value="">Kalimantan Barat</option>
		</select>
		<div class="clearfix"></div>
	</div>
	<!-- s:peta -->
	<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d8160886.443161523!2d121.9695609132859!3d-2.8717490758759427!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1420684759428" width="1200" height="300" frameborder="0" style="border:0" class="peta2"></iframe>
	<!-- e:peta -->
	<div class="clearfix pt30"></div>
	<!-- s:member -->
	<div class="container">
		<div class="list_member">
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Wakil Direksi</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Wakil Direksi</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Wakil Direksi</h6>
			</a>
		</div>
		<div class="clearfix"></div>
		<div class="t-center">
			<div class="paging">
				<a href="#">« PREV</a>
				<a href="#" class="selected">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#">4</a>
				<a class="range">...</a>
				<a href="#">7</a>
				<a href="#">8</a>
				<a href="#">NEXT »</a>
			</div>
		</div>
	</div>
	<!-- e:member -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>