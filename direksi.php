<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="pengelola";?>
<?php include "includes/head.php";?>

<body>
	<?php include "includes/header.php";?>
	<div class="container container_nowp2">
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Pengelola</a> /
			<a href="#">Direksi</a>
		</div>
		<h1 class="title title2">Direksi</h1>
	</div>
	<div class="clearfix pt30"></div>
	<!-- s:member -->
	<div class="container">
		<div class="list_member">
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Wakil Direksi</h6>
			</a>
			
		</div>
		<div class="clearfix pt30 pb30"></div>
	</div>
	<!-- e:member -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>