<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="pengelola";?>
<?php include "includes/head.php";?>

<body>
	<?php include "includes/header.php";?>
	<div class="container container_nowp2">
		<div class="breadcrumb">
			<a href="#">Home</a> / 
			<a href="#">Pengelola</a> /
			<a href="#">Anggota</a>
		</div>
		<h1 class="title title2">Struktur Organisasi</h1>
	</div>

	<div class="clearfix"></div>
	<!-- s:member -->
	<div class="container">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet erat id turpis imperdiet pharetra. Aenean risus augue, ornare sed finibus sit amet, vehicula non eros. Aliquam finibus magna eu risus aliquam tristique. Nunc a bibendum odio. Morbi suscipit, ligula ac egestas aliquet, augue ipsum pretium libero, sit amet porta ligula erat in risus. Pellentesque dictum neque sapien, vitae sodales nisi faucibus sit amet. Proin non dolor odio. Suspendisse nec lectus quam. Donec at ante et sem scelerisque sollicitudin. Quisque interdum fermentum accumsan. Curabitur accumsan vehicula lacus in semper. Donec vitae sapien sed lorem tempor consequat vel a velit.
		<div class="clearfix pt30"></div>
		<div class="list_member">
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Wakil Direksi</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Direksi Utama</h6>
			</a>
			<a alt="member_detail.php" class="box_modal">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Wakil Direksi</h6>
			</a>6>Wakil Direksi</h6>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- e:member -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>