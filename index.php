<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="home";?>
<?php include "includes/head.php";?>

<?php 
if($_GET['theme']=="cyan"){
?>
	<style>
	#header,
	#footer {
		background-color: rgba(0,174,239,0.8) !important;
	}
	#header #nav a,
	#footer a,
	#footer {
		color: #000;
	}
	.logo img {
		filter: brightness(0%);
	-webkit-filter: brightness(0%);
	-moz-filter: brightness(0%);
	-o-filter: brightness(0%);
	-ms-filter: brightness(0%);
	}
	</style>
<?php
}
elseif($_GET['theme']=="red"){
?>
	<style>
	#header,
	#footer {
		background-color: rgba(198,2,2,0.8) !important;
	}
	#header #nav a,
	#footer a,
	#footer {
		color: #fff;
	}
	.logo img {
		filter: brightness(100%);
	-webkit-filter: brightness(100%);
	-moz-filter: brightness(100%);
	-o-filter: brightness(100%);
	-ms-filter: brightness(100%);
	}
	</style>
<?php
}
else{}
	?>
<body>
	<?php include "includes/header.php";?>
	<!-- s:peta -->
	<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d8160886.443161523!2d121.9695609132859!3d-2.8717490758759427!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1420684759428" width="1200" height="500" frameborder="0" style="border:0" class="peta"></iframe>
	<!-- e:peta -->
	<!-- s:pengaduan -->
	<div class="box_pengaduan">
		<h3>Kirim Pengaduan</h3>
		<form action="#">
			<div>
				<h6>Nama</h6>
				<input type="text" class="input100">
			</div>
			<div class="clearfix pt5"></div>
			<div>
				<h6>Email</h6>
				<input type="text" class="input100">
			</div>
			<div class="clearfix pt5"></div>
			<div>
				<h6>Jenis Pengaduan</h6>
				<select name="" id="" class="input100">
					<option value=""></option>
					<option value=""></option>
				</select>
			</div>
			<div class="clearfix pt5"></div>
			<div>
				<h6>No. Tlp</h6>
				<input type="text" class="input100">
			</div>
			<div class="clearfix pt5"></div>
			<h6>Isi Pengaduan</h6>
			<textarea name="" id="" class="textarea100 h100"></textarea>
			<div class="clearfix pt5"></div>
			<input type="submit"  class="btn_save btn_smaller" value="Kirim">
		</form>
		<div class="list_bp">
			Atau Kirim melalui:
			<a href="pengaduan.php">Pengaduan Melalui Twitter</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- e:pengaduan -->
	<!-- s:berita -->
	<div class="container">
		<div class="title">Berita Terbaru</div>
		<div class="hl">
			<a href="detail.php" class="pic imgLiquid">
				<img src="img/01.jpg" alt="">
			</a>
			<h1><a href="detail.php">Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</a></h1>
			<span class="date">July 23, 2013</span>
			Pengadilan Tata Usaha Negara DKI Jakarta menolak Gugatan PT. Perusahaan Perkebunan Tratak (PT. Tratak) terhadap Badan Pertanahan Nasional yang mencabut HGU perusahaan tersebut melalui Keputusan Badan Pertanahan Nasional No. 7/PTT-HGU/BPN RI/2013 tentang Penetapan
		</div>
		<div class="list_berita list_berita_wp">
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/02.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/03.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/04.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
			<a href="detail.php">
				<div class="pic imgLiquid"><img src="img/img_default.jpg" alt=""></div>
				<div class="text">
					<div>
						<span class="date">24 Desember 2014</span>
						<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
					</div>
				</div>
				<div class="clearfix"></div>
			</a>
		</div>
		<div class="clearfix pt30"></div>
	</div>
	<div align="center" class="a_pengaduan">
		<a href="pengaduan.php" class="load_btn">Kirim Pengaduan</a>
	</div>
	<!-- e:berita -->
	<div class="center_wp">
		<div class="container_fluit">
			<div class="title t-center">Penanganan Kasus</div>
			<div class="list_center">
				<a href="berita.php?cate=hutan">
					<div class="pic imgLiquid"><img src="img/06.jpg" alt=""></div>
					<div class="label">HUTAN</div>
					<div class="text">
						<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
						<div class="date">July 23, 2013</div>
					</div>
				</a>
				<a href="berita.php?cate=perkebunan">
					<div class="pic imgLiquid"><img src="img/07.jpg" alt=""></div>
					<div class="label">perkebunan</div>
					<div class="text">
						<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
						<div class="date">July 23, 2013</div>
					</div>
				</a>
				<a href="berita.php?cate=pertambangan">
					<div class="pic imgLiquid"><img src="img/08.jpg" alt=""></div>
					<div class="label">pertambangan</div>
					<div class="text">
						<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
						<div class="date">July 23, 2013</div>
					</div>
				</a>
				<a href="berita.php?cate=kebebasan&#32;ekspresi">
					<div class="pic imgLiquid"><img src="img/09.jpg" alt=""></div>
					<div class="label">kebebasan ekspresi</div>
					<div class="text">
						<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
						<div class="date">July 23, 2013</div>
					</div>
				</a>
				<a href="berita.php?cate=isu&#32;lainnya">
					<div class="pic imgLiquid"><img src="img/10.jpg" alt=""></div>
					<div class="label">isu lainnya</div>
					<div class="text">
						<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
						<div class="date">July 23, 2013</div>
					</div>
				</a>
			</div>
			<div class="clearfix pt20"></div>
		</div>
	</div>
	<!-- s:somed -->
	<div class="sosmed">
		<div class="container">
			<div class="title t-center">Social Media</div>
			<div class="clearfix pt20"></div>
			<div class="list_sosmed">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-width="400" data-height="400" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
			</div>
			<div class="list_sosmed">
				<a class="twitter-timeline" href="https://twitter.com/twitter" data-widget-id="556978240229609473">Tweets by @twitter</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
			<div class="clearfix pt20"></div>
		</div>
	</div>
	<!-- e:sosmed -->
	<!-- s:member -->
	<div class="container">
		<div class="title t-center">Sekretariat Nasional</div>
		<div class="list_member">
			<a href="member.php">
				<div class="pic imgLiquid"><img src="img/m1.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Jawa Barat</h6>
			</a>
			<a href="member.php">
				<div class="pic imgLiquid"><img src="img/m2.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Jawa Barat</h6>
			</a>
			<a href="member.php">
				<div class="pic imgLiquid"><img src="img/m3.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Jawa Barat</h6>
			</a>
			<a href="member.php">
				<div class="pic imgLiquid"><img src="img/m4.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Jawa Barat</h6>
			</a>
			<a href="member.php">
				<div class="pic imgLiquid"><img src="img/m5.jpg" alt=""></div>
				<h4>Joko Suseno</h4>
				<h6>Jawa Barat</h6>
			</a>
		</div>
	</div>
	<!-- e:member -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>