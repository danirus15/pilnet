<?php error_reporting(E_ALL & ~E_NOTICE);?>
<html>
<?php $page=="kampanye";?>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/header.php";?>
	<div class="hl_kanal">
		<!-- s:berita -->
		<div class="container">
			<div class="breadcrumb">
				<a href="#">Home</a> / 
				<a href="#">Kasus</a>
			</div>
			<div class="title fl">
				Kasus
			</div>
			<select name="" id="" class="pilihberita">
				<option value="">Semua Kasus</option>
				<option value="">Resume kasus</option>
				<option value="">Analisa kasus</option>
				<option value="">Kronologis Kasus</option>
			</select>
			<div class="clearfix"></div>
			<div class="hl">
				<a href="detail.php" class="pic imgLiquid">
					<img src="img/11.jpg" alt="">
				</a>
				<h1><a href="detail.php">Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</a></h1>
				<span class="date">July 23, 2013</span>
				Pengadilan Tata Usaha Negara DKI Jakarta menolak Gugatan PT. Perusahaan Perkebunan Tratak (PT. Tratak) terhadap Badan Pertanahan Nasional yang mencabut HGU perusahaan tersebut melalui Keputusan Badan Pertanahan Nasional No. 7/PTT-HGU/BPN RI/2013 tentang Penetapan
			</div>
			<div class="list_berita list_berita_wp">
				<a href="detail.php">
					<div class="pic imgLiquid"><img src="img/07.jpg" alt=""></div>
					<div class="text">
						<div>
							<span class="date">24 Desember 2014</span>
							<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
						</div>
					</div>
					<div class="clearfix"></div>
				</a>
				<a href="detail.php">
					<div class="pic imgLiquid"><img src="img/06.jpg" alt=""></div>
					<div class="text">
						<div>
							<span class="date">24 Desember 2014</span>
							<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
						</div>
					</div>
					<div class="clearfix"></div>
				</a>
				<a href="detail.php">
					<div class="pic imgLiquid"><img src="img/04.jpg" alt=""></div>
					<div class="text">
						<div>
							<span class="date">24 Desember 2014</span>
							<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
						</div>
					</div>
					<div class="clearfix"></div>
				</a>
				<a href="detail.php">
					<div class="pic imgLiquid"><img src="img/05.jpg" alt=""></div>
					<div class="text">
						<div>
							<span class="date">24 Desember 2014</span>
							<h2>Siaran Pers Hari Pendidikan Nasional 2 Mei 2012</h2>
						</div>
					</div>
					<div class="clearfix"></div>
				</a>
			</div>
			<div class="clearfix pt30"></div>
		</div>
	</div>
	<div class="clearfix pt20"></div>
	<div class="container">
		<div class="list_center list_center2">
			<a href="berita.php?cate=hutan">
				<div class="pic imgLiquid"><img src="img/img_default.jpg" alt=""></div>
				<div class="label">Resume kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=perkebunan">
				<div class="pic imgLiquid"><img src="img/07.jpg" alt=""></div>
				<div class="label">Kronologis Kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=pertambangan">
				<div class="pic imgLiquid"><img src="img/08.jpg" alt=""></div>
				<div class="label">Kronologis Kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
					
				</div>
			</a>
			<a href="berita.php?cate=kebebasan&#32;ekspresi">
				<div class="pic imgLiquid"><img src="img/09.jpg" alt=""></div>
				<div class="label">Resume kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=isu&#32;lainnya">
				<div class="pic imgLiquid"><img src="img/10.jpg" alt=""></div>
				<div class="label">Analisa kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=hutan">
				<div class="pic imgLiquid"><img src="img/11.jpg" alt=""></div>
				<div class="label">Resume kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=perkebunan">
				<div class="pic imgLiquid"><img src="img/01.jpg" alt=""></div>
				<div class="label">Analisa kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=pertambangan">
				<div class="pic imgLiquid"><img src="img/02.jpg" alt=""></div>
				<div class="label">pertambangan</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
					
				</div>
			</a>
			<a href="berita.php?cate=kebebasan&#32;ekspresi">
				<div class="pic imgLiquid"><img src="img/03.jpg" alt=""></div>
				<div class="label">Kronologis Kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=isu&#32;lainnya">
				<div class="pic imgLiquid"><img src="img/04.jpg" alt=""></div>
				<div class="label">Analisa kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=hutan">
				<div class="pic imgLiquid"><img src="img/11.jpg" alt=""></div>
				<div class="label">Analisa kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<a href="berita.php?cate=perkebunan">
				<div class="pic imgLiquid"><img src="img/01.jpg" alt=""></div>
				<div class="label">Kronologis Kasus</div>
				<div class="text">
					<div class="date2">July 23, 2013</div>
					<h3>Siaran Pers Bersama: Redistribusi Lahan eks – PT Tratak Harus Segera Dilaksanakan</h3>
				</div>
			</a>
			<div class="clearfix"></div>
		</div>
		<div align="center">
				<a href="#" class="load_btn">Load More</a>
			</div>
	</div>
	<!-- e:berita -->
	<?php include "includes/footer.php";?>
</body>
<?php include "includes/js.php";?>
</html>